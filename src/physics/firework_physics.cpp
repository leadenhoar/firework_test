
#include "src/engine/engine.h"
#include "src/physics/firework_physics.h"

namespace firework {


	std::atomic<int> Firework::free_cells;
	std::atomic<size_t> Firework::exploised_during_frame;
	std::atomic<size_t> Firework::rendered_living_count;


	Firework::Firework() :
		particle_buffer(new Particle[MAX_PARTICLES]),
		particle_back_buffer(new Particle[MAX_PARTICLES])
	{
		free_cells = MAX_PARTICLES;
		exploised_during_frame = 0;
		rendered_living_count = 0;

		for (size_t i = 0; i < MAX_PARTICLES; ++i)
		{
			particle_buffer[i].fire = this;
			particle_back_buffer[i].fire = this;
		}
	}
	
	Firework::~Firework()
	{
		std::lock_guard<std::recursive_mutex> lslock(particle_back_buffer_mutex);
		delete[] particle_buffer;
		delete[] particle_back_buffer;
	}
		

	bool Firework::spawn_explosion(Vecf p, size_t thread_number, size_t thread_count, 
		enums::ParticleType type, float part_of_particles, float part_living_time)
	{
		if (buffer_balancer.free_count(thread_number) < size_t(PARTICLES_IN_EXPLOSION * part_of_particles)) return false;

		for (size_t count = 0; count < size_t(PARTICLES_IN_EXPLOSION * part_of_particles); ++count)
		{
			size_t i = buffer_balancer.get_free(thread_number);
			particle_buffer[i].generate_simple(p);
			particle_buffer[i].type = type;
			particle_buffer[i].living_time = int(particle_buffer[i].living_time * part_living_time);
			buffer_balancer.set_locked(thread_number, i);
			//std::cout << "RISE in expl: : " << i << std::endl;
		}
		return true;
	}

	bool Firework::spawn_trace(Particle &parent_particle, size_t thread_number)
	{
		if (!(buffer_balancer.free_count(thread_number))) return false;

		size_t i = buffer_balancer.get_free(thread_number);
		particle_buffer[i].generate_simple(parent_particle.p);
		particle_buffer[i].v = parent_particle.v * 0.5;
		particle_buffer[i].living_time *= 3.0;
		buffer_balancer.set_locked(thread_number, i);

	}

	// TODO: check this
	int Firework::living_check(size_t cur_update_index) // use after update cur index
	{
		int counter = 0;
		for (size_t i = 0; i <= MAX_PARTICLES; ++i)
			counter += (int)(particle_buffer[i].living);
		return MAX_PARTICLES - counter;
	}

	void Firework::swap_particle_buffers()
	{
		std::lock_guard<std::recursive_mutex> lock(particle_back_buffer_mutex);
		std::swap(particle_buffer, particle_back_buffer);
	}

	void Firework::render(VertInd &vert_ind_renderer_buffer)
	{
		//rendered_living_count = 0;
		std::lock_guard<std::recursive_mutex> lock(particle_back_buffer_mutex);
		for (size_t i = 0; i < MAX_PARTICLES; ++i) 
		{
			particle_back_buffer[i].render(vert_ind_renderer_buffer);
		}
		//if (rendered_living_count < MAX_PARTICLES / 10)
		//	std::cout << "ALARM!!!! STRANGE RENDER";
		//std::cout << "rendered_living_count: " << rendered_living_count << std::endl;
		rendered_living_count = 0;
		
	}

	void Firework::update(int dt,  size_t thread_number, size_t thread_count) //milliseconds 
	{

		while (buffer_balancer.unsync_count(thread_number))
		{
			size_t i = buffer_balancer.get_unsync(thread_number);
			assert(i < MAX_PARTICLES);
			particle_buffer[i].living = false;
		}

		exploised_during_frame = 0;
		while (buffer_balancer.locked_count(thread_number)) 
		{
			size_t i = buffer_balancer.get_locked(thread_number);
			assert(i < MAX_PARTICLES);
			if (particle_buffer[i].update_from_last(dt, i, thread_number, thread_count))
				buffer_balancer.set_locked(thread_number, i);
			else
			{
				buffer_balancer.set_free(thread_number, i);
				buffer_balancer.set_unsync(thread_number, i);
			}
		}
			//int true_free = living_check(i);
			//if (true_free != free_cells)
			//	std::cout << "ALARM!!!!!!" << true_free<< " != " << free_cells << std::endl;
					
	}

	void Firework::generate()
	{
		if (GENERATION_PART < common_math::PRECISION) return;
		size_t worker_num = 0;
		for (size_t _ = 0; _ < size_t(MAX_PARTICLES*GENERATION_PART); ++_)
		{
			worker_num %= effect_dispatchern::WORKERS_COUNT;
			if (!buffer_balancer.free_count(worker_num)) continue;
			size_t i = buffer_balancer.get_free(worker_num);
			auto &si = particle_buffer[i];
			si.generate_simple();
			buffer_balancer.set_locked(worker_num, i);
			worker_num++;
			new (particle_back_buffer + i) Particle(si);
		}
	}

}