#pragma once

#include <mutex>
#include <atomic>
#include <math.h>

#include "src/types/vec.h"
#include "src/util/util.h"
#include "src/settings.h"
#include "src/physics/balancer.h"
#include "src/types/color.h"
#include "src/util/rand_.h"
#include "src/physics/particle.h"
#include "src/containers/vertex_index_struct.h"
#include "src/types/enums.h"

namespace firework {

	using Vecf = Vec2::Vec2<float>;
	using urgba = rgba;
	struct Particle;

	struct Firework 
	{

		Particle *particle_buffer, *particle_back_buffer;
		static std::atomic<int> free_cells;
		std::recursive_mutex particle_back_buffer_mutex;
		balancer::Balancer buffer_balancer;
		static std::atomic<size_t> exploised_during_frame;

		static std::atomic<size_t> rendered_living_count;

		
		Firework();

		~Firework();

		bool spawn_explosion(Vecf p, size_t thread_number, size_t thread_count,
			enums::ParticleType type = enums::ParticleType::simple,
			float part_of_particles = 1.f, float part_living_time = 1.f);
		
		bool spawn_trace(Particle &p, size_t thread_number);

		int living_check(size_t cur_update_index = 0);

		void swap_particle_buffers();

		void render(VertInd &vert_ind_renderer_buffer);

		void update(int dt, size_t thread_number = 0, size_t thread_count = 1); //milliseconds

		void generate();
	};
}