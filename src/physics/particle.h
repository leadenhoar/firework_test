#pragma once

#include "src/types/color.h"
#include "src/types/vec.h"
#include "src/physics/firework_physics.h"
#include "src/containers/vertex_index_struct.h"
#include "src/types/enums.h"

namespace firework {
	
	using Vecf = Vec2::Vecf;
	struct Firework;

	struct Particle {
		Vecf p, v, a;
		rgba c;
		bool living;
		Firework *fire;
		int living_time;
		int living_time_cur;
		int living_time_max;
		enums::ParticleType type;

		Particle() :
			p(Vecf()),
			a(Vecf(PARTICLE_VX_FADING_ACCELERATION, GRAVITY)),
			living(false),
			fire(nullptr),
			living_time_cur(0),
			living_time_max(RANDOM_LIVING_TIME_DURATION ?
			(PARTICLE_MIN_LIVING_TIME + rand()) % PARTICLE_MAX_LIVING_TIME : PARTICLE_MAX_LIVING_TIME),
			type(enums::ParticleType::simple)
		{
			c.rand();
			living_time = living_time_max;
		}

		Particle(Vecf p_, Vecf v_, Vecf a_, enums::ParticleType type_) :
			p(p_),
			v(v_),
			a(a_),
			living(false),
			living_time_cur(0),
			living_time_max(RANDOM_LIVING_TIME_DURATION ?
			(PARTICLE_MIN_LIVING_TIME + rand()) % PARTICLE_MAX_LIVING_TIME : PARTICLE_MAX_LIVING_TIME),
			type(type_)
		{
			living_time = living_time_max;
		}

		Particle(Particle const & pt) :
			p(pt.p), v(pt.v),
			a(pt.a), c(pt.c),
			living(pt.living),
			fire(pt.fire),
			living_time(pt.living_time),
			living_time_cur(pt.living_time_cur),
			living_time_max(pt.living_time_max),
			type(pt.type)

		{}

		void render(VertInd &vert_ind_renderer_buffer);

		bool update_from_last(int dt, size_t num_in_buf, size_t thread_number, size_t thread_count);

		void die(int num_in_buf);

		void generate_simple(Vecf p = Vecf(0.f, 0.f));

	};

}