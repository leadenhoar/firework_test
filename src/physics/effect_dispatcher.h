#pragma once

#include <mutex>
#include <atomic>
#include <vector>
#include <numeric> 
#include <memory>

#include "src/types/vec.h"
#include "src/settings.h"
#include "src/physics/firework_physics.h"
#include "src/containers/cycled_static_array.h"
#include "src/containers/vertex_index_struct.h"

namespace effect_dispatchern
{
	enum class WorkerState
	{
		worker_func_begin_wall,
		worker_func_end_wall
	};

	enum class WorkingState
	{
		working, 
		worker_must_exit,
		dispatcher_must_exit
	};

	struct EffectDispatcher
	{
		static std::atomic<WorkerState> worker_state;
		static std::atomic<WorkingState> working_state;

		static std::atomic<int> global_time;
		static std::atomic<int> workers_dt;
		
		static std::atomic<size_t> waiting_workers;

		static CycledStaticArray<size_t, 100> render_mspf, physics_mspf, explosions;

		static std::shared_ptr<firework::Firework> fire;
		
		EffectDispatcher();

		~EffectDispatcher();

		static void dispatcher_function(EffectDispatcher *dispatcher);

		static void worker_function(firework::Firework *fire, size_t thread_number, size_t thread_count);

		void init_effects();

		void init();

		void update(int dt);

		void term();

		void render(VertInd &vert_ind_renderer_buffer);
	};
}