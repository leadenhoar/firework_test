
#include "effect_dispatcher.h"
#include "src/input/input_sys.h"
#include "src/types/enums.h"

namespace effect_dispatchern
{
	
	std::atomic<WorkerState> EffectDispatcher::worker_state;
	std::atomic<WorkingState> EffectDispatcher::working_state;

	std::atomic<int> EffectDispatcher::global_time;
	std::atomic<int> EffectDispatcher::workers_dt;

	std::atomic<size_t> EffectDispatcher::waiting_workers;

	CycledStaticArray<size_t, 100> EffectDispatcher::render_mspf, EffectDispatcher::physics_mspf, EffectDispatcher::explosions;

	std::shared_ptr<firework::Firework> EffectDispatcher::fire;

	EffectDispatcher::EffectDispatcher()
	{
		worker_state = WorkerState::worker_func_begin_wall;

		global_time = 0;
		workers_dt = 0;

		working_state = WorkingState::working;

		waiting_workers = WORKERS_COUNT;
		fire = nullptr;
	}

	EffectDispatcher::~EffectDispatcher()
	{
		term();
	}

	void EffectDispatcher::worker_function(firework::Firework *fire, size_t thread_number, size_t thread_count)
	{
		while (working_state != WorkingState::worker_must_exit)
		{
			waiting_workers--;
			while (worker_state == WorkerState::worker_func_begin_wall)
				std::this_thread::yield();

			fire->update(workers_dt, thread_number, thread_count);

			waiting_workers++;
			while (worker_state == WorkerState::worker_func_end_wall)
				std::this_thread::yield();

		}
	}

	void EffectDispatcher::dispatcher_function(EffectDispatcher *dispatcher)
	{
		while (working_state != WorkingState::dispatcher_must_exit)
		{
			if (waiting_workers == 0)
				worker_state = WorkerState::worker_func_end_wall;
			
			if (waiting_workers == WORKERS_COUNT)
			{
				static int lastTime = 0;
				int time = global_time.load();
				int delta = time - lastTime;

				if (delta < 10)
					std::this_thread::sleep_for(std::chrono::milliseconds(10 - delta));
				else
				{
					lastTime = time;
					input_sys::swap_buffers();
					
					static size_t index = 0;
					size_t step = firework::MAX_PARTICLES / WORKERS_COUNT;
					while (!input_sys::is_empty())
					{
						auto particles_type = static_cast<enums::ParticleType>(rand() %
							int(enums::ParticleType::last));
						//particles_type = enums::ParticleType::sub_expl;
						index %= WORKERS_COUNT;
						fire->spawn_explosion(input_sys::get_first() , index++, 1, particles_type);
					}

					dispatcher->fire->swap_particle_buffers();
					dispatcher->fire->buffer_balancer.swap_locked_cells();

					if(fire->exploised_during_frame)
						explosions.push_back(fire->exploised_during_frame);

					workers_dt = delta;
					worker_state = WorkerState::worker_func_begin_wall;

					physics_mspf.push_back(size_t(delta));
					static int nextStats = -1;
					if (nextStats < time)
					{	
						std::cout << "===================>FPS(avg) Physics: " << int((float)1000 / physics_mspf.avg()) << std::endl;
						std::cout << "Exploised_during_frame: " << int(explosions.avg()) << std::endl;
						std::cout << "Physics Avg ms/f: " << int(physics_mspf.avg()) << std::endl;
						std::cout << "Physics Max ms/f: " << physics_mspf.max_element() << std::endl;
						std::cout << "Physics Min ms/f: " << physics_mspf.min_element() << std::endl;
						std::cout << "Free cells: " << fire->free_cells << std::endl;
						std::cout << "Lock cells: " << firework::MAX_PARTICLES - fire->free_cells << std::endl;

						//std::cout << "FreeStack cells: " << dispatcher->fire->buffer_balancer.free_count(0) << std::endl;
						//std::cout << "UnsynckStack cells: " << dispatcher->fire->buffer_balancer.unsync_count(0) << std::endl;
						//std::cout << "LockStack cells: " << dispatcher->fire->buffer_balancer.locked_count(0) << std::endl;
						//std::cout << "LockBackStack cells: " << dispatcher->fire->buffer_balancer.locked_back_count(0) << std::endl;
						nextStats = (int)time + FPS_INFO_UPDATE;
					}
				}
			}
		}
		working_state = WorkingState::worker_must_exit;
	}

	void EffectDispatcher::init_effects()
	{
		fire = std::make_shared<firework::Firework>();
		fire->generate();
		for (size_t i = 0; i < WORKERS_COUNT; ++i)
				std::thread(worker_function, fire.get(), i, WORKERS_COUNT).detach();

	}

	void EffectDispatcher::init()
	{
		init_effects();
		std::thread(dispatcher_function, this).detach();
	}

	void EffectDispatcher::update(int dt)
	{
		int time = global_time.load();
		global_time.store(time + dt);
	}

	void EffectDispatcher::term()
	{
		working_state = WorkingState::dispatcher_must_exit;
	}

	void EffectDispatcher::render(VertInd &vert_ind_renderer_buffer)
	{
		static int lastTime = 0;
		int time = global_time.load();
		int delta = time - lastTime;
		lastTime = time;
		render_mspf.push_back(size_t(delta));
		fire->render(vert_ind_renderer_buffer);
		
		static int next_stats = -1;
		if (next_stats < time)
		{
			std::cout << "===================>FPS(avg) Render : " << int((float)1000 / render_mspf.avg()) << std::endl;
			next_stats = (int)time + FPS_INFO_UPDATE;
		}
	}

}