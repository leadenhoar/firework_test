#pragma once

#include <stdint.h>

#include "src/containers/static_stack.h"
#include "src/util/util.h"
#include "src/settings.h"

namespace balancer
{
	constexpr int32_t ceil(float num)
	{
		return (static_cast<float>(static_cast<int32_t>(num)) == num)
			? static_cast<int32_t>(num)
			: static_cast<int32_t>(num) + ((num > 0) ? 1 : 0);
	}

	constexpr float max_particles = (const float)firework::MAX_PARTICLES;
	constexpr size_t balancer_size = ceil(max_particles / effect_dispatchern::WORKERS_COUNT);
	using Stack_ = StaticStack<size_t, balancer_size>;

	class Balancer
	{
	public:

		Balancer();

		~Balancer();

		size_t get_free(size_t worker_num);

		size_t get_locked(size_t worker_num);

		size_t get_unsync(size_t worker_num);

		void set_unsync(size_t worker_num, size_t value);

		void set_locked(size_t worker_num, size_t value);

		void set_free(size_t worker_num, size_t value);

		size_t free_count(size_t worker_num);

		size_t locked_count(size_t worker_num);

		size_t unsync_count(size_t worker_num);

		size_t locked_back_count(size_t worker_num);

		void swap_locked_cells();

	private:
		Stack_ *free_cells;
		Stack_ *unsync_cells;
		Stack_ *locked_cells_back;
		Stack_ *locked_cells;
	};
}