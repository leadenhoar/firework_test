
#include "src/physics/balancer.h"


namespace balancer
{
	Balancer::Balancer()
	{
		free_cells = new Stack_[effect_dispatchern::WORKERS_COUNT]();
		unsync_cells = new Stack_[effect_dispatchern::WORKERS_COUNT]();
		locked_cells_back = new Stack_[effect_dispatchern::WORKERS_COUNT]();
		locked_cells = new Stack_[effect_dispatchern::WORKERS_COUNT]();

		for (size_t i = 0; i < firework::MAX_PARTICLES; ++i)
		{
			size_t worker_num = i / balancer_size;
			free_cells[worker_num].push(i);
		}
	}

	Balancer::~Balancer()
	{
		delete[] free_cells;
		delete[] unsync_cells;
		delete[] locked_cells_back;
		delete[] locked_cells;
	}

	size_t Balancer::get_free(size_t worker_num)
	{
		return free_cells[worker_num].top_pop();
	}

	size_t Balancer::get_locked(size_t worker_num)
	{
		return locked_cells[worker_num].top_pop();
	}

	size_t Balancer::get_unsync(size_t worker_num)
	{
		return unsync_cells[worker_num].top_pop();
	}

	void Balancer::set_unsync(size_t worker_num, size_t value)
	{
		unsync_cells[worker_num].push(value);
	}

	void Balancer::set_locked(size_t worker_num, size_t value)
	{
		locked_cells_back[worker_num].push(value);
	}

	void Balancer::set_free(size_t worker_num, size_t value)
	{
		free_cells[worker_num].push(value);
	}

	size_t Balancer::free_count(size_t worker_num)
	{
		return free_cells[worker_num].current_size();
	}

	size_t Balancer::locked_count(size_t worker_num)
	{
		return locked_cells[worker_num].current_size();
	}	
	
	size_t Balancer::unsync_count(size_t worker_num)
	{
		return unsync_cells[worker_num].current_size();
	}

	size_t Balancer::locked_back_count(size_t worker_num)
	{
		return locked_cells_back[worker_num].current_size();
	}

	void Balancer::swap_locked_cells()
	{
		std::swap(locked_cells, locked_cells_back);
	}


}