
#include "src/settings.h"
#include "src/physics/particle.h"
#include "src/engine/engine.h"
#include "src/renderer/renderer.h"


namespace firework
{
	void Particle::render(VertInd &vert_ind_renderer_buffer)
	{
		if (!living) return;
		vert_ind_renderer_buffer.draw_point(p.x, p.y, c.r, c.g, c.b, c.a);
		++fire->rendered_living_count;
	}


	bool Particle::update_from_last(int dt, size_t num_in_buf, size_t thread_number, size_t thread_count)
	{
		auto const &last = fire->particle_back_buffer[num_in_buf];

		living = true;
		living_time_cur = last.living_time_cur + dt;
		living_time_max = last.living_time_max;
		float dts = (float)dt / 1e3f;

		Vecf dv = last.a * dts;

		// Air resistance
		float tmp_vx = abs(last.v.x) - abs(dv.x);
		if (tmp_vx > common_math::PRECISION)
			v.x = (last.v.x < 0 ? -1.f : 1.f) * tmp_vx;

		// Gravity
		v.y = last.v.y + dv.y;
		p = last.p + v * dts;

		type = last.type;

		if (!p.inbox(int(config::SCREEN_WIDTH), int(config::SCREEN_HEIGHT)))
		{
			//std::cout << "DIED: !Inbox: " << num_in_buf << std::endl;
			die(num_in_buf);
			return false;
		}

		switch (last.type)
		{
		case enums::ParticleType::simple:
			break;

		case enums::ParticleType::sub_expl:
		{
			if (util::prob_rand(SUB_EXPLOSION_CHANCE))
				fire->exploised_during_frame += int(fire->spawn_explosion(p, thread_number, thread_count, 
					enums::ParticleType::simple, SUB_EXPLOSION_PART, SUB_EXPLOSION_TIME));
			break;
		}
		case enums::ParticleType::trace:
		{
			
			if (util::prob_rand(TRACE_CHANCE))
				fire->spawn_trace(*this,thread_number);
			break;
		}
		
		default:
			break;
		}

		if (living_time_cur > living_time)
		{
			if (util::prob_rand(EXPLOSION_CHANCE))
				fire->exploised_during_frame += int(fire->spawn_explosion(p, thread_number, thread_count));
			//else
			{
				die(num_in_buf);
				return false;
			}
			//std::cout << "DIED: !Time: " << num_in_buf << std::endl;
		}
		return true;
	}

	void Particle::die(int num_in_buf)
	{
		living = false;
		living_time_cur = 0;
		fire->free_cells++;
		if (fire->free_cells > MAX_PARTICLES)
		{
			//int true_free = fire->living_check(num_in_buf);
			std::cout << "ALARM!!!! FREE_CELLS LEAK" << std::endl;
		}
	}

	void Particle::generate_simple(Vecf p_)
	{
		if (p_ == Vecf(0.f, 0.f))
			p = Vecf(util::frand() * config::SCREEN_WIDTH, util::frand() * (float)config::SCREEN_HEIGHT);
		else
			p = p_;

		v = Vecf((util::frand() - 0.5f), (util::frand() - 0.5f));
		v.normalize();
		float mul = (float)((MIN_SPAWN_SPEED + rand()) % MAX_SPAWN_SPEED);
		v *= mul;

		living = true;
		fire->free_cells--;
		living_time = living_time_max;
		living_time_cur = 0;
		type = enums::ParticleType::simple;
		if (fire->free_cells < 0)
			std::cout << "ALARM!!!! FREE_CELLS LEAK" << std::endl;
	}

}