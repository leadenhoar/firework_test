#include "input_sys.h"

namespace input_sys
{
	StaticQueue<Vec2::Vecf, firework::MAX_EXPLOSIONS>  user_input;
	StaticQueue<Vec2::Vecf, firework::MAX_EXPLOSIONS>  user_input_last;



	void init(GLFWwindow* window)
	{
		if (!window) return;
		glfwSetMouseButtonCallback(window, mouse_button_callback);
	}

	void on_click(int x, int y)
	{
		user_input.push_back(Vec2::Vecf((float)x, (float)(config::SCREEN_HEIGHT - y)));
	}

	void handle_mouse(GLFWwindow* window)
	{
		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		on_click(int(xpos), int(ypos));
		//std::cout << "x: " << xpos << " y: " << ypos << "### all: " << user_input << std::endl;

	}

	void handle_custom(GLFWwindow* window)
	{

	}

	void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
	{
		if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
			handle_mouse(window);
		
		/*if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
			handle_custom(window);*/

		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(window, true);
	}

	void swap_buffers()
	{
		user_input.swap(user_input_last);
	}

	bool is_empty()
	{
		return user_input_last.empty();
	}

	Vec2::Vecf get_first()
	{
		return user_input_last.get_first();
	}
}
