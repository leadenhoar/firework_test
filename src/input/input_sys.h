#pragma once

#include "src/containers/static_queue.h"
#include "src/types/vec.h"
#include "src/physics/firework_physics.h"
#include "src/renderer/renderer.h"

namespace input_sys
{
	void init(GLFWwindow* window);

	void on_click(int x, int y);

	void handle_mouse(GLFWwindow* window);

	void handle_custom(GLFWwindow* window);

	void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

	void swap_buffers();

	bool is_empty();

	Vec2::Vecf get_first();
	
}