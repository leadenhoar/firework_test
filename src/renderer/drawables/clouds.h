#pragma once

#include <glm/vec2.hpp>

#include "drawable.h"
#include "src/settings.h"
#include "src/util/timer/timer.h"

class Clouds : public Drawable
{
public:
	Clouds(
		bool is_active_,
		const char* vertexPath,
		const char* fragmentPath,
		int vert_count_,
		int attr_len_,
		int ind_count_,
		const float* vertices_ = nullptr,
		const unsigned int* indices_ = nullptr) :

		Drawable(
			is_active_,
			vertexPath,
			fragmentPath,
			vert_count_,
			attr_len_,
			ind_count_,
			vertices_,
			indices_)
	{}

	virtual void init_buf()
	{
		// set up vertex data (and buffer(s)) and configure vertex attributes
		// ------------------------------------------------------------------
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);

		// bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(*vert_ind->vertices) * vert_count * attr_len, vert_ind->vertices, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(*vert_ind->indices) * vert_count, vert_ind->indices, GL_STATIC_DRAW);

	}

	virtual void init_attr()
	{
		// position attribute
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		// color attribute
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		// texture coord attribute
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));

		shader.setFloat("time", 8.f);
		//glm::vec2 resolution = glm::vec2(config::SCREEN_WIDTH, config::SCREEN_WIDTH);
		//shader.set_vec2("resolution", resolution);
	}

	virtual void render()
	{
		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(*vert_ind->vertices) * vert_count * attr_len, vert_ind->vertices);
		//glBindBuffer(GL_ARRAY_BUFFER, 0);


		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(*vert_ind->indices) * vert_count, vert_ind->indices);
		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		glVertexAttribPointer(0, attr_len, GL_FLOAT, GL_FALSE, attr_len * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);

		shader.use();

		float tmp_time = (float)glob_timer::get().time / 1000000.f;
		//tmp_time = tmp_time / std::powf(1.05f, tmp_time);
		///tmp_time = tmp_time > 5.f ? 0.f : tmp_time;
		//std::cout << "TIME: " << tmp_time << std::endl;
		shader.setFloat("time", tmp_time);

		//glm::vec2 resolution = glm::vec2(config::SCREEN_WIDTH, config::SCREEN_WIDTH);
		//shader.set_vec2("resolution", resolution);

		// render container
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}

	virtual void update(int dt)
	{
		
	}
};