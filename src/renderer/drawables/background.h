#pragma once

#include "drawable.h"

class Background : public Drawable
{
protected:
	std::string tex_path;
	unsigned int texture1;
public:

	Background(
		bool is_active_,
		const char* vertexPath,
		const char* fragmentPath,
		int vert_count_,
		int attr_len_,
		int ind_count_,
		const char* tex_path_,
		const float* vertices_ = nullptr,
		const unsigned int* indices_ = nullptr) :

		Drawable(
			is_active_,
			vertexPath,
			fragmentPath,
			vert_count_,
			attr_len_,
			ind_count_,
			vertices_,
			indices_),
		texture1(0), tex_path(tex_path_)
		
	{}

	virtual void init_buf()
	{
		// set up vertex data (and buffer(s)) and configure vertex attributes
		// ------------------------------------------------------------------
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);

		// bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(*vert_ind->vertices) * vert_count * attr_len, vert_ind->vertices, GL_STREAM_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(*vert_ind->indices) * vert_count, vert_ind->indices, GL_STREAM_DRAW);

	}

	virtual void init_attr()
	{
		// position attribute
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);
		// color attribute
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);
		// texture coord attribute
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glEnableVertexAttribArray(2);

	}

	virtual void init_texture()
	{
		// load and create a texture 
		// -------------------------
		// texture 1
		// ---------
		glGenTextures(1, &texture1);
		glBindTexture(GL_TEXTURE_2D, texture1);
		// set the texture wrapping parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// set texture wrapping to GL_REPEAT (default wrapping method)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// load image, create texture and generate mipmaps
		int width, height, nrChannels;
		stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
												// The FileSystem::getPath(...) is part of the GitHub repository so we can find files on any IDE/platform; replace it with your own image path.
		unsigned char *data = stbi_load(tex_path.c_str(), &width, &height, &nrChannels, STBI_rgb_alpha);
		if (data)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else
		{
			std::cout << "Failed to load texture" << std::endl;
		}
		stbi_image_free(data);
		
		// tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
		// -------------------------------------------------------------------------------------------
		shader.use(); // don't forget to activate/use the shader before setting uniforms!
						 // either set it manually like so:
		glUniform1i(glGetUniformLocation(shader.ID, "texture1"), 0);
		// or set it via the texture class
		//shader.setInt("texture1", 1);
	}

	virtual void render()
	{
		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(*vert_ind->vertices) * vert_count * attr_len, vert_ind->vertices);
		//glBindBuffer(GL_ARRAY_BUFFER, 0);


		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(*vert_ind->indices) * vert_count, vert_ind->indices);
		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		glVertexAttribPointer(0, attr_len, GL_FLOAT, GL_FALSE, attr_len * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);
			
		// bind textures on corresponding texture units
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture1);
			

		// render container
		shader.use();

		//glEnable(GL_ALPHA_TEST);
		//glAlphaFunc(GL_LESS, 0.5f);

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}
	
};
