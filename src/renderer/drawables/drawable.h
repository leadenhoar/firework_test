#pragma once

#include <memory>
#include <stb_image.h>
#include <stb_image_write.h>

#include "src/renderer/shader.h"
#include "src/containers/vertex_index_struct.h"
#include "src/settings.h"

class Drawable
{
protected:
	bool is_active__;
	Shader shader;
	int vert_count;
	int attr_len;
	int ind_count;

	uint8_t* jpeg_buffer;

	unsigned int VBO, VAO, EBO;

	std::shared_ptr<VertInd> vert_ind;

public:

	Drawable(
		bool is_active_,
		const char* vertexPath, 
		const char* fragmentPath,
		unsigned int vert_count_, 
		int attr_len_,
		int ind_count_,
		const float* vertices_ = nullptr, 
		const unsigned int* indices_= nullptr) :

		is_active__(is_active_),
		vert_count(vert_count_),
		attr_len(attr_len_),
		ind_count(ind_count_),
		VBO(0), VAO(0), EBO(0)
	{
		shader = Shader(vertexPath, fragmentPath);

		vert_ind = std::make_shared<VertInd>(vert_count,
			attr_len,
			ind_count,
			vertices_ ,
			indices_);
		
		jpeg_buffer = new uint8_t[vert_count_ * 4];
	}

	bool is_active()
	{
		return is_active__;
	}

	Drawable() :
		is_active__(0),
		vert_count(0),
		attr_len(0),
		ind_count(0),
		VBO(0), VAO(0), EBO(0),
		jpeg_buffer(nullptr)
	{}

	void swap(Drawable &value)
	{
		std::swap(is_active__, value.is_active__);
		std::swap(vert_count, value.vert_count);
		std::swap(attr_len, value.attr_len);
		std::swap(ind_count, value.ind_count);
		std::swap(VBO, value.VBO);
		std::swap(VAO, value.VAO);
		std::swap(EBO, value.EBO);
		std::swap(jpeg_buffer, value.jpeg_buffer);
	}

	Drawable(const Drawable &value) :
		is_active__(value.is_active__),
		vert_count(value.vert_count),
		attr_len(value.attr_len),
		ind_count(value.ind_count),
		VBO(value.VBO), VAO(value.VAO), EBO(value.EBO),
		jpeg_buffer(new uint8_t[value.vert_count * 4])
	{
		std::memcpy(jpeg_buffer, value.jpeg_buffer, sizeof(uint8_t) * vert_count * 4);
	}

	Drawable &operator=(const Drawable &value)
	{
		if (this == &value) return *this;
		Drawable(value).swap(*this);
	}

	virtual void init_buf()
	{
		// set up vertex data (and buffer(s)) and configure vertex attributes
		// ------------------------------------------------------------------
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);

		// bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(*vert_ind->vertices) * vert_count * attr_len, vert_ind->vertices, GL_STREAM_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(*vert_ind->indices) * vert_count, vert_ind->indices, GL_STREAM_DRAW);

	}

	virtual void init_attr()
	{
		// TODO: first attr_len mb not valid
		glVertexAttribPointer(0, attr_len, GL_FLOAT, GL_FALSE, attr_len * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);

		// note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
		glBindBuffer(GL_ARRAY_BUFFER, 0);


		// remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,	0);

		// You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
		// VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
		glBindVertexArray(0);

		// wireframe mode
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	virtual void init_texture()
	{
	}

	virtual void init()
	{
		shader.init();
		init_buf();
		init_attr();
		init_texture();
	}

	virtual void update(int dt) {}

	virtual void render() {}

	virtual void term()
	{
		// optional: de-allocate all resources once they've outlived their purpose:
		// ------------------------------------------------------------------------
		glDeleteVertexArrays(1, &VAO);
		glDeleteBuffers(1, &VBO);
		glDeleteBuffers(1, &EBO);
	}

	void dump_to_file(char const *filename)
	{
		auto vi = vert_ind;

		int b = 0; //which byte to write to
		for (int j = 0; j < config::SCREEN_HEIGHT; j++)
		{
			for (int i = 0; i < config::SCREEN_WIDTH; i++)
			{
				jpeg_buffer[b++] = 0;
				jpeg_buffer[b++] = 0;
				jpeg_buffer[b++] = 0;
				jpeg_buffer[b++] = 0;
			}

			for (int i = 0; i < vi->cur_vert_count; ++i)
			{
				int x = (int)((vi->vertices[i * 3] + 1.f) / 2 * config::SCREEN_WIDTH);
				int y = (int)((vi->vertices[i * 3 + 1] + 1.f) / 2 * config::SCREEN_HEIGHT);
				int index = (y * config::SCREEN_WIDTH + x) * 4;
				jpeg_buffer[index] = 250;
			}
		}

		stbi_write_jpg(filename, config::SCREEN_WIDTH, config::SCREEN_HEIGHT, 4, jpeg_buffer, 95);
	}

	~Drawable()
	{
		delete[] jpeg_buffer;
	}
};