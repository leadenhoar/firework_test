#pragma once

#include <memory>
#include <functional>

#include "drawable.h"
#include "src/settings.h"
#include "src/physics/effect_dispatcher.h"

class Firework : public Drawable
{
private:
	std::shared_ptr<effect_dispatchern::EffectDispatcher> effect_dispatcher;

public:

	Firework(
		bool is_active_,
		const char* vertexPath,
		const char* fragmentPath,
		int vert_count_,
		int attr_len_,
		int ind_count_,
		const float* vertices_ = nullptr,
		const unsigned int* indices_ = nullptr) :

		Drawable(
			is_active_,
			vertexPath,
			fragmentPath,
			vert_count_,
			attr_len_,
			ind_count_,
			vertices_,
			indices_),
			effect_dispatcher(std::make_shared<effect_dispatchern::EffectDispatcher>())
	{
	}

	virtual void init()
	{
		Drawable::init();
		effect_dispatcher->init();
	}

	virtual void term()
	{
		Drawable::term();
		effect_dispatcher->term();
	}

	virtual void init_attr()
	{
		// position attribute
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, attr_len * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);
		// color attribute
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, attr_len * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);

	}


	virtual void render()
	{
		effect_dispatcher->render(*vert_ind);
		
		glBindVertexArray(VAO); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
								//glDrawArrays(GL_TRIANGLES, 0, 6);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(*vert_ind->vertices) * vert_count * attr_len, vert_ind->vertices);
		//glBindBuffer(GL_ARRAY_BUFFER, 0);


		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(*vert_ind->indices) * vert_count, vert_ind->indices);
		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		// position attribute
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, attr_len * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);
		// color attribute
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, attr_len * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);

		shader.use();

		
		glDrawElements(GL_POINTS, vert_ind->cur_vert_count, GL_UNSIGNED_INT, 0);

		glBindVertexArray(0);
		vert_ind->cur_vert_count = 0;

	}

	virtual void update(int dt)
	{
		effect_dispatcher->update(dt);
	}
};