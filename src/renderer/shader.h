#pragma once

#include <glad/glad.h>
#include <glm/vec2.hpp>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include "src/types/vec.h"

class Shader
{
public:
	unsigned int ID;
	std::string vertex_path, fragment_path;
	// constructor generates the shader on the fly
	// ------------------------------------------------------------------------
	Shader(const char* vertexPath = "", const char* fragmentPath = "");

	void init();

	// activate the shader
	// ------------------------------------------------------------------------
	void use();

	// utility uniform functions
	// ------------------------------------------------------------------------
	void setBool(const std::string &name, bool value) const;

	void setInt(const std::string &name, int value) const;

	void setFloat(const std::string &name, float value) const;

	void set_vec2(const std::string &name, glm::vec2 &value) const;

private:
	// utility function for checking shader compilation/linking errors.
	// ------------------------------------------------------------------------
	void checkCompileErrors(unsigned int shader, std::string type);
};
