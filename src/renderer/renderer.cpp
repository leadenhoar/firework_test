
#include "renderer.h"

#define STB_IMAGE_IMPLEMENTATION //only place once in one .cpp file
#include <stb_image.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION //only place once in one .cpp files
#include <stb_image_write.h>

#include "drawables/firework.h"
#include "drawables/background.h"
#include "drawables/clouds.h"

namespace renderer
{
	
	GLFWwindow* window = nullptr;

	std::vector<std::shared_ptr<Drawable>> drawables;
	
	const int BUFFER_SIZE = config::SCREEN_HEIGHT * config::SCREEN_WIDTH;
	
	void init_drawables()
	{


		const float background_array[] = {
			// positions                     // texture coords
			1.0f,  1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
			1.0f, -1.0f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // bottom right
			-1.0f, -1.0f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // bottom left
			-1.0f,  1.0f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // top left 
		};

		const float clouds_array[] = {
			// positions          // colors           // texture coords
			1.0f,  1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
			1.0f, -1.0f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // bottom right
			-1.0f, -1.0f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // bottom left
			-1.0f,  1.0f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // top left 
		};

		const unsigned int background_indices[] = {
			0, 1, 3, // first triangle
			1, 2, 3  // second triangle
		};


		

		drawables.push_back(std::shared_ptr<Drawable>(new Background(
			true,
			"src/shaders/background.vs",
			"src/shaders/background.fs",
			sizeof(background_array) / sizeof(background_array[0]),
			8,
			sizeof(background_array) / sizeof(background_array[0]),
			"resources/textures/background_only.png",
			background_array,
			background_indices
		)));

		drawables.push_back(std::shared_ptr<Drawable>(new Clouds(
			true,
			"src/shaders/clouds.vs",
			"src/shaders/clouds.fs",
			sizeof(clouds_array) / sizeof(clouds_array[0]),
			8,
			sizeof(clouds_array) / sizeof(clouds_array[0]),
			clouds_array,
			background_indices
		)));

		drawables.push_back(std::shared_ptr<Drawable>(new Firework(
			true,
			"src/shaders/base2d.vs",
			"src/shaders/firework.fs",
			BUFFER_SIZE,
			7,
			BUFFER_SIZE)));

		drawables.push_back(std::shared_ptr<Drawable>(new Background(
			true,
			"src/shaders/background.vs",
			"src/shaders/background.fs",
			sizeof(background_array) / sizeof(background_array[0]),
			8,
			sizeof(background_array) / sizeof(background_array[0]),
			"resources/textures/city_only.png",
			background_array,
			background_indices
		)));
	}


	int glfw_init()
	{
		// glfw: initialize and configure
		// ------------------------------
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		// glfw window creation
		// --------------------
		window = glfwCreateWindow(config::SCREEN_WIDTH, config::SCREEN_HEIGHT, "Firework", NULL, NULL);
		if (window == NULL)
		{
			std::cout << "Failed to create GLFW window" << std::endl;
			glfwTerminate();
			return -1;
		}
		glfwMakeContextCurrent(window);
		glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
		return 0;
	}

	int init()
	{
		// glad: load all OpenGL function pointers
		// ---------------------------------------
		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		{
			std::cout << "Failed to initialize GLAD" << std::endl;
			return -1;
		}

		// configure global opengl state
		// -----------------------------
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		init_drawables();
		for (auto drawable : drawables)
			if(drawable->is_active()) drawable->init();

		return 0;
	}

	bool render()
	{
		if (glfwWindowShouldClose(window)) return false;

		glClearColor(0.f, 0.f, 0.f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		for (auto drawable : drawables)
			if (drawable->is_active()) drawable->render();


		// glBindVertexArray(0); // no need to unbind it every time 
		//std::cout << "cur_vert_count: " << vert_ind->cur_vert_count << std::endl;

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);
		glfwPollEvents();

		return true;
	}

	void update(int dt)
	{
		for (auto drawable : drawables)
			if (drawable->is_active()) drawable->update(dt);
	}



	void term()
	{
		for (auto drawable : drawables)
			if (drawable->is_active()) drawable->term();

		// glfw: terminate, clearing all previously allocated GLFW resources.
		// ------------------------------------------------------------------
		glfwTerminate();

	}

	// glfw: whenever the window size changed (by OS or user resize) this callback function executes
	// ---------------------------------------------------------------------------------------------
	void framebuffer_size_callback(GLFWwindow* window, int width, int height)
	{
		// make sure the viewport matches the new window dimensions; note that width and 
		// height will be significantly larger than specified on retina displays.
		glViewport(0, 0, width, height);
	}

}


