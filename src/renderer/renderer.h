#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <memory>
#include <vector>

//#define STBI_WRITE_NO_STDIO
#include "src/engine/engine.h"
#include "src/renderer/drawables/drawable.h"


namespace renderer
{
	extern GLFWwindow* window;
	
	void framebuffer_size_callback(GLFWwindow* window, int width, int height);

	extern std::vector<std::shared_ptr<Drawable>> drawables;

	int glfw_init();

	int init();

	bool render();

	void update(int dt);

	void term();

	void framebuffer_size_callback(GLFWwindow* window, int width, int height);


}

