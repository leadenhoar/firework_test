#pragma once

#include <math.h>
#include <iostream>
#include <assert.h>
#include <iomanip>
#include <type_traits>

#include "src/math/common_math.h"

namespace Vec2 
{
	template<typename T>
	struct Vec2 
	{
		T x, y;

		Vec2() : x(0.f), y(0.f) {};

		Vec2(T x_, T y_) : x(x_), y(y_) {}

		Vec2 operator+(Vec2 const &rh) const
		{
			return Vec2(x + rh.x, y + rh.y);
		}
		
		Vec2 operator-(Vec2 const &rh)  const
		{
			return Vec2(x - rh.x, y - rh.y);
		}

		Vec2 &operator+=(Vec2 const &rh)
		{
			x += rh.x;
			y += rh.y;
			return *this;
		}

		Vec2 &operator-=(Vec2 const &rh)
		{
			x -= rh.x;
			y -= rh.y;
			return *this;
		}

		Vec2 operator*(T scal) const
		{
			return Vec2(x * scal, y * scal);
		}

		Vec2 &operator*=(T scal) 
		{
			x *= scal;
			y *= scal;
			return *this;
		}

		T norm() 
		{
			return sqrt(x * x + y * y);
		}

		Vec2& normalize() 
		{
			float n = norm();
			x = float(x) / n;
			y = float(y) / n;
			return *this;
		}

		bool operator==(Vec2 &rh)  const
		{
			//std::cout << std::fixed << std::setprecision(10) << PRECISION << std::endl;
			//std::cout << std::fixed << std::setprecision(10) << x - rh.x << std::endl;
			//std::cout << std::fixed << std::setprecision(10) << abs(y - rh.y) << std::endl;
			return (((T)abs(x - rh.x) < (T)common_math::PRECISION) && ((T)abs(y - rh.y) < (T)common_math::PRECISION));
		}

		bool operator==(Vec2 &&rh)  const
		{
			//std::cout << std::fixed << std::setprecision(10) << PRECISION << std::endl;
			//std::cout << std::fixed << std::setprecision(10) << x - rh.x << std::endl;
			//std::cout << std::fixed << std::setprecision(10) << abs(y - rh.y) << std::endl;
			return (((T)abs(x - rh.x) < (T)common_math::PRECISION) && ((T)abs(y - rh.y) < (T)common_math::PRECISION));
		}

		bool operator !=(Vec2 &rh) const
		{
			return !(*this == rh);
		}

		bool inbox(T xb, T yb) const
		{
			return 0.f < x && x < xb && 0.f < y && y < yb;
		}
	};

	template<typename T>
	std::ostream &operator<<(std::ostream& out, Vec2<T>& v) 
	{
		if (std::is_floating_point<T>::value)
			out << "(" << v.x << ", " << v.y << ")";
		else
			out << "(" << std::fixed << std::setprecision(3) << v.x << ", " << v.y << ")";


		return out;
	}

	template<typename T>
	void test_template(int num, T& lh, T& rh) 
	{
		bool result = (lh == rh);
		/*std::cout << num << (!result ? " : faled" : " 0k") << std::endl;
		std::cout << lh << std::endl << rh << std::endl;*/
		assert(result);
	}

	template<typename T>
	void test_template(int num, T&& lh, T&& rh)
	{
		bool result = (lh == rh);
		/*std::cout << num << (!result ? " : faled" : " 0k") << std::endl;
		std::cout << lh << std::endl << rh << std::endl;*/
		assert(result);
	}

	void vec2_tests();

	using Vecf = Vec2<float>;
}


