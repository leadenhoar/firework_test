
#include "src/types/color.h"

rgba::rgba()
{
	r = 0.f;
	g = 0.f;
	b = 0.f;
	a = 1.f;
}

void rgba::rand()
{
	r = util::fbrand();
	g = util::fbrand();
	b = util::fbrand();
	a = util::fbrand();
}
