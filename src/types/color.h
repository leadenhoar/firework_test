#pragma once

#include "src/util/rand_.h"

struct rgba {
	float r, g, b, a;

	rgba();
	void rand();
};