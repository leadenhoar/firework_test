
#include <iostream>

#include <src/types/vec.h>

namespace Vec2
{
	void vec2_tests()
	{
		Vecf a(1, 1), b(2, 2), c(3, 4);
		std::cout << "vec2 tests" << std::endl;
		std::cout << "======================" << std::endl;

		int num = 0;
		test_template(num++, a + b, Vecf(3, 3));
		test_template(num++, a - b, Vecf(-1, -1));
		test_template(num++, Vecf(a += b), Vecf(3, 3));
		test_template(num++, Vecf(a), Vecf(3, 3));
		test_template(num++, Vecf(a -= a), Vecf());
		test_template(num++, Vecf(c.normalize()), Vecf(0.6f, 0.8f));

		std::cout << "======================" << std::endl;
	}
}

