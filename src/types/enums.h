#pragma once

namespace enums 
{

enum class ParticleType
{
	simple, sub_expl, trace, // big_expl,
	last
};

}
