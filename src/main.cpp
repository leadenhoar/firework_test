
//#define _CRT_SECURE_NO_WARNINGS
//#pragma warning (disable : 4996)

#include "src/engine/engine.h"

int main()
{
	engine::init();
	engine::run();
	engine::term();
}