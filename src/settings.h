#pragma once

namespace config
{
	static constexpr int SCREEN_WIDTH = 1024; //1024 1920
	static constexpr int SCREEN_HEIGHT = 768; //768 1080
}

namespace firework {

	static const float GRAVITY = -25.f;

	static const int PARTICLES_IN_EXPLOSION = 3200;
		// with 64  optimal: EXPLOSION_CHANCE = 0.025f;
		// with 128  optimal: EXPLOSION_CHANCE = 0.01f;
		// with 256 optimal: EXPLOSION_CHANCE = 0.008f;

	static const int MAX_EXPLOSIONS = 400;
	static const int MAX_PARTICLES = PARTICLES_IN_EXPLOSION * MAX_EXPLOSIONS;

	static const float GENERATION_PART = 0.0f;

	static const int MAX_SPAWN_SPEED = 1 << 8;
	static const int MIN_SPAWN_SPEED = 100000000;

	// Very sensitive param
	static const float EXPLOSION_CHANCE = 0.0001f;  //probability as part of 1(every roll)
	static const float SUB_EXPLOSION_CHANCE = 0.05f;  
	static const float SUB_EXPLOSION_TIME = 0.05f;  
	static const float SUB_EXPLOSION_PART = 0.05f;  
	static const float TRACE_CHANCE = 0.6f;

	// Air resistance
	const float PARTICLE_VX_FADING_ACCELERATION = 2.5f;
	const float MIN_VX = 0;
	
	const bool RANDOM_LIVING_TIME_DURATION = true;
		// with false optimal: EXPLOSION_CHANCE <= 0.06f; 
		// with true  optimal: EXPLOSION_CHANCE >= 0.02f; 

	const int PARTICLE_MAX_LIVING_TIME = 1000; //miliseconds
	const int PARTICLE_MIN_LIVING_TIME = 500; //miliseconds
}

namespace effect_dispatchern
{
	const size_t WORKERS_COUNT = 6;
	static const size_t FPS_INFO_UPDATE = 2000;
}