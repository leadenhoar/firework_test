
#include "engine.h"

void engine::init(void)
{
	renderer::glfw_init();
	input_sys::init(renderer::window);
}

void engine::term(void)
{
}

void engine::run(void)
{
	renderer::init();
	// render loop
	// -----------
	do
	{
		engine::update();
		engine::etc_render();
	} while (renderer::render());
	renderer::term();
}

void engine::etc_render(void)
{
	if (cur_frames_count_glob < 420)
	{
		std::string path = std::string("debug_frames/frame") +
			std::to_string(cur_frames_count_glob) +
			std::string(".jpg");

		const char* cpath = path.c_str();

		//render::dump_to_file(cpath);
	}
}


void engine::update()
{
	// dt in milliseconds
	int dt = glob_timer::get().dt;
	renderer::update(dt);

	static int cur_frames_count = 0;
	static int dt_accum = 0;
	cur_frames_count++;
	cur_frames_count_glob++;
	dt_accum += dt;

	if (dt_accum >= 5000)
	{
		std::cout 
			<< "=============================>Update_fps:"
			<< (float)1000.f / dt_accum * cur_frames_count << std::endl
			<< "=============================>Frame: " 
			<< cur_frames_count_glob << std::endl;

		cur_frames_count = 0;
		dt_accum = 0;

	}
	//std::cout << dt_array.avg() << std::endl;
	//dt_array.push_back(dt);
}

