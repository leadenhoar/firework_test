#pragma once

#include <string>

#include "src/settings.h"
#include "src/containers/cycled_static_array.h"
#include "src/renderer/renderer.h"
#include "src/input/input_sys.h"
#include "src/util/timer/timer.h"

namespace engine
{
	static int cur_frames_count_glob = 0;

	static auto dt_array = CycledStaticArray<int, 1000>();

	void etc_render(void);

	void update(); 

	void init(void);

	void term(void);

	void run(void);
};