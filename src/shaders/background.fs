#version 330 core
vec4 tmpColor;
out vec4 FragColor;
  
in vec3 ourColor;
in vec2 TexCoord;

uniform sampler2D texture1;

void main()
{
    tmpColor = texture(texture1, TexCoord);
	//if (tmpColor.a < 0.5) discard;
	FragColor = tmpColor;
}