#pragma once

#include <GLFW/glfw3.h>

struct dt_time{
	int dt;
	int time;

	dt_time(int dt_, int time_) : dt(dt_), time(time_) {}
};

namespace glob_timer
{
	//milliseconds
	dt_time  get();
}
