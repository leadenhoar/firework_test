#include "timer.h"

dt_time glob_timer::get()
{
	static int lastTime = 0;
	static int global_time = 0;

	int time = int(glfwGetTime() * 1000); //milliseconds
										  //std::cout << time << std::endl;
	int delta = time - lastTime;
	lastTime = time;
	global_time += time;

	return dt_time(delta, global_time);
}