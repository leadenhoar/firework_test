#include "src/util/rand_.h"


namespace util {

	bool prob_rand(float prob)
	{
		int num = distribution(random_gen);
		return  num < int(1000.f * prob);
	}

	float frand()
	{
		return (float)rand() / (float)RAND_MAX;
	}

	float fbrand()
	{
		int c = rand() % 256;
		c += 32;
		c = c > 255 ? 255 : c;
		return (float)c / 190.f;
	}

	unsigned long xorshf96_rand(void) {          //period 2^96-1
		unsigned long t;
		x ^= x << 16;
		x ^= x >> 5;
		x ^= x << 1;

		t = x;
		x = y;
		y = z;
		z = t ^ x ^ y;

		return z;
	}

	size_t trand()
	{
		return size_t(clock());
		//return std::rand();
	}
}