#pragma once

#include <random>
#include <ctime>

namespace util {

	static std::random_device rd;  //Will be used to obtain a seed for the random number engine
	static std::mt19937 random_gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	static std::uniform_int_distribution<> distribution(1, 1000);
	static unsigned long x = 123456789, y = 362436069, z = 521288629;

	bool prob_rand(float prob);

	float fbrand();

	float frand();

	unsigned long xorshf96_rand(void);

	size_t trand();

}