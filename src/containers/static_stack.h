#pragma once

#include <array>
#include <assert.h>

template <typename T, size_t N>
class StaticStack
{
public:
	StaticStack() : top_(0) {};

	~StaticStack() {};

	void push(const T &value)
	{
		assert(top_ < N);
		data_[top_++] = value;
	}

	T &top()
	{
		return data_[top_ - 1];
	}

	void pop()
	{
		assert(top_);
		top_--;
	}

	T &top_pop()
	{
		assert(top_);
		return data_[top_-- - 1];
	}

	size_t current_size()
	{
		return top_;
	}

private:
	std::array<T, N> data_;
	size_t top_;
};