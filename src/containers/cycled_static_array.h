#pragma once

#include <numeric>
#include <algorithm>

template <typename T, size_t N>
class CycledStaticArray
{
public:

	CycledStaticArray() : cur_end(0), cur_num(0) {}

	void push_back(T e)
	{
		data[cur_end++] = e;
		cur_end %= N;
		cur_num = std::min(++cur_num, N);
	}

	T accumulate()
	{
		return std::accumulate(data.begin(), data.begin() + cur_num, (T)0);
	}

	T min_element()
	{
		return *std::min_element(data.begin(), data.begin() + cur_num);
	}

	T max_element()
	{
		return *std::max_element(data.begin(), data.begin() + cur_num);
	}

	T avg()
	{
		if (!(T)cur_num) return T(0);
		return T(accumulate() / (T)cur_num);
	}

private:
	std::array<T, N> data;
	size_t cur_end;
	size_t cur_num;

};
