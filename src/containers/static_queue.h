#pragma once

#include <mutex>


template <typename T, size_t N>
class StaticQueue
{
public:

	StaticQueue() :size(0)
	{
		data = new T[N];
	}

	~StaticQueue()
	{
		std::lock_guard<std::mutex> lock(data_mutex_);
		delete[] data;
	}

	bool empty()
	{
		return !(bool)size;
	}

	T get_first()
	{
		std::lock_guard<std::mutex> lock(data_mutex_);
		//assert(size);
		size--;
		T &tmp = data[first_num++];
		first_num %= N;
		return tmp;
	}

	void push_back(T e)
	{
		std::lock_guard<std::mutex> lock(data_mutex_);
		data[(first_num + size) % N] = e;
		if (size == N)
		{
			++first_num;
			first_num %= N;
		}
		else
		{
			++size;
		}
	}

	void swap(StaticQueue &rq)
	{
		std::lock_guard<std::mutex> lock(data_mutex_);
		std::swap(data, rq.data);
		std::swap(size, rq.size);
		std::swap(first_num, rq.first_num);
	}

	std::ostream &dump(std::ostream &out)
	{
		//std::lock_guard<std::recursive_mutex> lock(q.data_mutex_);
		out << "+++++++++++++++++++++++++++++++++++> ";
		out << "size:" << size << " ";
		for (size_t i = first_num; i < first_num + size; ++i)
			out << data[i % N] << " ";
		return out;
	}

private:
	T * data;
	size_t first_num;
	size_t size; // like .end()
	std::mutex data_mutex_;

};

template <typename T, size_t N>
std::ostream &operator<<(std::ostream &out, StaticQueue<T, N> &q)
{
	return q.dump(out);
}