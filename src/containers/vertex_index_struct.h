#pragma once

#include <memory>

class VertInd
{
public:

	float *vertices;
	unsigned int *indices;
	
	int cur_vert_count;
	int vert_count;
	int attr_len;

	VertInd(int vert_count_, int attr_len_, int ind_count, const float* vertices_, const unsigned int* indices_)
		:
		attr_len(attr_len_),
		vertices(new float[vert_count_ * attr_len_]),
		indices(new unsigned int[ind_count]),
		cur_vert_count(0), 
		vert_count(vert_count_)
	{
		if (!vertices_ && !indices_)
		{
			std::memset(vertices, 0, sizeof(float) * vert_count * attr_len);
			std::memset(indices, 0, sizeof(unsigned int) * vert_count);
		}
		else
		{
			std::memcpy(vertices, vertices_, sizeof(float) * vert_count * attr_len);
			std::memcpy(indices, indices_, sizeof(unsigned int) * vert_count);
		}
	};

	void draw_point(float x, float y, float r, float g, float b, float a)
	{
		if (cur_vert_count > vert_count)
		{
			std::cout << "ERROR::FIREWORK_drawable::VERTIND::OVERFLOW_VERT_COUNT: " << cur_vert_count << std::endl;
			return;
		}
		else
		{
			float x_ = 2.f * x / (float)config::SCREEN_WIDTH - 1.f;
			float y_ = 2.f * y / (float)config::SCREEN_HEIGHT - 1.f;

			//std::cout << x_ << ",  " << y_ << std::endl;
			//std::this_thread::sleep_for(std::chrono::milliseconds(1000));

			const float tmp[7] = { x_, y_, 0.f, r, g, b, a };

			for (int i = 0; i < std::min(attr_len, 7); ++i)
				vertices[cur_vert_count * attr_len + i] = tmp[i];

			indices[cur_vert_count] = cur_vert_count;
		}
		cur_vert_count++;
	}

	~VertInd()
	{
		delete[] vertices;
		delete[] indices;
	}

};